import { authLogout } from '@/utils/auth'

export default function({ $axios, redirect, app }) {
  // $axios.setToken(token, 'JWT')
  // console.log('adasdasdas', token, 'JWT')
  $axios.onRequest((config) => {
    const token = app.$cookies.get('token') ? app.$cookies.get('token') : null
    config.headers.common.Authorization = 'JWT ' + token
  })

  $axios.onError((error) => {
    // const code = parseInt(error.response && error.response.status)
    if (error.response) {
      if (
        (error.response.data.detail === 'Signature has expired.' &&
          error.response.status === 403) ||
        (error.response.data.detail === 'Signature has expired.' &&
          error.response.status === 401) ||
        (error.response.data.detail ===
          'Usted no tiene permiso para realizar esta acción.' &&
          error.response.status === 403) ||
        (error.response.data.detail === 'Invalid signature.' &&
          error.response.status === 403) ||
        (error.response.data.detail === 'Error decoding signature.' &&
          error.response.status === 403)
      ) {
        authLogout()
        redirect('/ingresar')
      }
    }
  })
}
// if ((error.response.status === 401 && !originalRequest._retry) ||
//       (error.response.data.detail === 'Signature has expired.' && error.response.status === 403) ||
//       (error.response.data.detail === 'Usted no tiene permiso para realizar esta acción.' && error.response.status === 403) ||
//       (error.response.data.detail === 'Invalid signature.' && error.response.status === 403)) {
//       console.log(token)
//       authLogout()
//       window.location.href = '/'

//     }
