export const state = () => {
  return {
    pages: []
  }
}
export const getters = {
  getPages(state) {
    return state.pages
  }
}
export const mutations = {
  setPages(state, pages) {
    state.pages = pages
  }
}

export const actions = {}
