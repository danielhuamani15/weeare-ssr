export const state = () => {
  return {
    baseUrlMedia: 'https://weeare.pe',
    today: '',
    menu: false
  }
}
export const getters = {
  BaseUrlMedia(state) {
    return state.baseUrlMedia
  },
  getToday(state) {
    return state.today
  }
}
export const mutations = {
  setToday(state, today) {
    state.today = today
  },
  toggleMenu(state, value) {
    state.menu = value
  }
}
export const actions = {
  nuxtServerInit({ commit }, { req, app }) {
    const auth = app.$cookies.get('token') ? app.$cookies.get('token') : null
    const firstName = app.$cookies.get('first_name')
    const lastName = app.$cookies.get('last_name')
    const today = new Date().getTime()
    commit('auth/setFullName', `${firstName} ${lastName}`)
    commit('auth/setAuth', auth)
    commit('setToday', today)
    const pages = app.$cookies.get('pages')
    // if (!pages) {
    //   console.log('aaaaaaaaaaaaaa.')
    //   app.$axios
    //     .$get('block/pages', {
    //       params: {
    //         'fields!': 'content'
    //       }
    //     })
    //     .then((response) => {
    //       console.log(response, 'adasda')
    //       commit('page/setPages', response)
    //     })
    // }
  }
}
