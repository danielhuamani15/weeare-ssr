export const state = () => {
  return {
    auth: null,
    fullName: ''
  }
}
export const getters = {
  getAuth(state) {
    return state.auth
  },
  getFullName(state) {
    return state.fullName
  }
}
export const mutations = {
  setAuth(state, auth) {
    state.auth = auth
  },
  setFullName(state, fullName) {
    state.fullName = fullName
  }
}

export const actions = {
  setAuth({ commit }, value) {
    commit('setAuth', value)
  },
  setFullName({ commit }, value) {
    commit('setFullName', value)
  }
}
